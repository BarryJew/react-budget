import { BudgetItemType, BudgetSliceState } from "../redux/budgetSlice";

export const getBudgetLS = () => {
  const json = window.localStorage.getItem("budget");
  if (json) {
    const data: BudgetItemType[] = JSON.parse(json);
    const budgetState: BudgetSliceState = {
      items: data,
    };
    return budgetState;
  }
  return {
    items: [],
  };
};
