import { transactionType } from "../utils/enums";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "./store";
import { getBudgetLS } from "../utils/getBudgetLS";

export type BudgetItemType = {
  id: number;
  comment: string;
  value: number;
  type: transactionType;
};

export interface BudgetSliceState {
  items: BudgetItemType[];
}

const initialState: BudgetSliceState = getBudgetLS();

const budgetSlice = createSlice({
  name: "budget",
  initialState,
  reducers: {
    addItem(state, action: PayloadAction<BudgetItemType>) {
      state.items.push(action.payload);
    },
    removeItem(state, action: PayloadAction<number>) {
      state.items = state.items.filter((obj) => obj.id !== action.payload);
    },
  },
});

export const selectBudgetItems = (state: RootState) => state.budget.items;

export const { addItem, removeItem } = budgetSlice.actions;

export default budgetSlice.reducer;
