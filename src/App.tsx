import React from "react";
import { FormBlock, TransactionCard } from "./components";
import { useSelector } from "react-redux";
import { BudgetItemType, selectBudgetItems } from "./redux/budgetSlice";

import "./scss/app.scss";
import { transactionType } from "./utils/enums";

function App() {
  const items: BudgetItemType[] = useSelector(selectBudgetItems);
  const [list, setList] = React.useState<BudgetItemType[]>(items);
  const [type, setType] = React.useState<transactionType>(transactionType.All);
  const isMounted = React.useRef<boolean>(false);

  React.useEffect(() => {
    if (isMounted.current) {
      const json = JSON.stringify(items);
      window.localStorage.setItem("budget", json);
    }

    isMounted.current = true;
  }, [items]);

  const setItems = (type?: transactionType) => {
    if (type === transactionType.All) {
      setList(items);
    } else {
      setList(items.filter((obj) => obj.type === type));
    }
  };

  React.useEffect(() => {
    setItems(type);
  }, [items, type]);

  const total: number = items.reduce((sum, item) => {
    if (item.type === transactionType.OUTCOME) {
      sum = sum - item.value;
    } else {
      sum += item.value;
    }
    return sum;
  }, 0);

  return (
    <div className="App">
      <FormBlock />
      <h2
        className={`total-balance ${
          total < 0
            ? "total-balance--red"
            : total > 0
            ? "total-balance--green"
            : ""
        }`}
      >
        Баланс: {total} ₽
      </h2>
      <div className="filter">
        <button
          onClick={() => setType(transactionType.OUTCOME)}
          className="filter__item"
        >
          Только расходы
        </button>
        <button
          onClick={() => setType(transactionType.INCOME)}
          className="filter__item"
        >
          Только доходы
        </button>
        <button
          onClick={() => setType(transactionType.All)}
          className="filter__item"
        >
          Показать все
        </button>
      </div>
      <div className="budget">
        <h2 className="budget__title">Список операций</h2>
        {list.length > 0 && (
          <div className="budget__list">
            {list.map((obj: BudgetItemType) => (
              <TransactionCard key={obj.id} {...obj} />
            ))}
          </div>
        )}
      </div>
    </div>
  );
}

export default App;
