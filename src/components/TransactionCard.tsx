import React from "react";
import { BudgetItemType, removeItem } from "../redux/budgetSlice";
import { transactionType } from "../utils/enums";
import { useAppDispatch } from "../redux/store";

const TransactionCard: React.FC<BudgetItemType> = ({
  id,
  comment,
  value,
  type,
}) => {
  const dispatch = useAppDispatch();
  const onRemoveTransaction = () => {
    if (window.confirm(`Вы точно хотите удалить операцию ${comment} ?`)) {
      dispatch(removeItem(id));
    }
  };
  return (
    <div className="budget-item">
      <h3 className="budget-item__comment">{comment}</h3>
      <p
        className={`budget-item__value ${
          type === transactionType.OUTCOME ? "budget-item__value--red" : ""
        }`}
      >
        {value} ₽
      </p>
      <svg
        className={`budget-item__icon ${
          type === transactionType.OUTCOME ? "budget-item__icon--outcome" : ""
        }`}
        xmlns="http://www.w3.org/2000/svg"
        width="24"
        height="24"
      >
        <path d="M17 10c-.3 0-.5-.1-.7-.3l-5-5c-.4-.4-.4-1 0-1.4s1-.4 1.4 0l5 5c.4.4.4 1 0 1.4-.2.2-.4.3-.7.3z" />
        <path d="M7 10c-.3 0-.5-.1-.7-.3-.4-.4-.4-1 0-1.4l5-5c.4-.4 1-.4 1.4 0s.4 1 0 1.4l-5 5c-.2.2-.4.3-.7.3z" />
        <path d="M12 21c-.6 0-1-.4-1-1V4c0-.6.4-1 1-1s1 .4 1 1v16c0 .6-.4 1-1 1z" />
      </svg>
      <button
        onClick={() => onRemoveTransaction()}
        className="budget-item__btn"
      >
        Удалить
      </button>
    </div>
  );
};
export default TransactionCard;
