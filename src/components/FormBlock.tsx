import React from "react";
import { transactionType } from "../utils/enums";
import { useAppDispatch } from "../redux/store";
import { addItem, BudgetItemType } from "../redux/budgetSlice";

const FormBlock: React.FC = () => {
  const dispatch = useAppDispatch();
  const [commentValue, setCommentValue] = React.useState<string>("");
  const [isOpen, setIsOpen] = React.useState<boolean>(false);
  const commentRef = React.useRef<HTMLInputElement>(null);
  const [sumValue, setSumValue] = React.useState<number>(0);
  const sumRef = React.useRef<HTMLInputElement>(null);
  const [actionType, setActionType] = React.useState<transactionType>(
    transactionType.INCOME
  );
  const [commentError, setCommentError] = React.useState<boolean>(false);
  const [sumError, setSumError] = React.useState<boolean>(false);
  const popupRef = React.useRef<HTMLDivElement>(null);

  const onSelect = (value: transactionType) => {
    setActionType(value);
    setIsOpen(false);
  };

  const onChangeSum = (sum: string) => {
    setSumError(false);
    const newValue = Number(sum);
    if (newValue) {
      setSumValue(newValue);
    } else {
      setSumError(true);
      setSumValue(0);
    }
  };

  const onChangeComment = (text: string) => {
    setCommentError(false);
    if (!text.trim().length) {
      setCommentError(true);
    }
    setCommentValue(text);
  };

  const addTransaction = () => {
    if (commentValue.length && sumValue) {
      const newItem: BudgetItemType = {
        id: Math.random(),
        comment: commentValue,
        value: sumValue,
        type: actionType,
      };
      dispatch(addItem(newItem));
      setCommentValue("");
      setSumValue(0);
    }
  };

  React.useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (
        popupRef.current &&
        !event.composedPath().includes(popupRef.current)
      ) {
        setIsOpen(false);
      }
    };

    document.body.addEventListener("click", handleClickOutside);

    return () => {
      document.body.removeEventListener("click", handleClickOutside);
    };
  }, []);

  return (
    <div className="form-card">
      <h3 className="form-card__input-title">Тип операции</h3>
      <div ref={popupRef} className="form-card__select">
        <p
          onClick={() => setIsOpen(true)}
          className="form-card__input form-card__input--select"
        >
          <span>{actionType}</span>
          <svg>
            <path
              fill="none"
              stroke="#000"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeMiterlimit="10"
              strokeWidth="2"
              d="m20.5 11.5-6 6-6-6"
            />
          </svg>
        </p>
        {isOpen && (
          <ul className="form-card__select-popup">
            <li
              onClick={() => onSelect(transactionType.INCOME)}
              className={actionType === transactionType.INCOME ? "active" : ""}
            >
              {transactionType.INCOME}
            </li>
            <li
              onClick={() => onSelect(transactionType.OUTCOME)}
              className={actionType === transactionType.OUTCOME ? "active" : ""}
            >
              {transactionType.OUTCOME}
            </li>
          </ul>
        )}
      </div>
      <div className="form-card__item">
        <h3 className="form-card__input-title">Комментарий</h3>
        <input
          ref={commentRef}
          value={commentValue}
          onChange={(event) => onChangeComment(event.target.value)}
          type="text"
          className={`form-card__input${commentError ? " error" : ""}`}
          required
        />
        {commentError && (
          <p className="form-card__error">Введите комментарий</p>
        )}
      </div>
      <div className="form-card__item">
        <h3 className="form-card__input-title">Сумма операции</h3>
        <input
          ref={sumRef}
          value={sumValue}
          onChange={(event) => onChangeSum(event.target.value)}
          type="text"
          className={`form-card__input${sumError ? " error" : ""}`}
          required
        />
        {sumError && <p className="form-card__error">Введите сумму больше 0</p>}
      </div>
      <button
        type="button"
        onClick={() => addTransaction()}
        className="form-card__btn"
      >
        Добавить
      </button>
    </div>
  );
};

export default FormBlock;
